using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaintAppication
{
    /// <summary>
    /// A part of Test Driven Development, a skeleton class is created.
    /// This class is intended to check the loop syntax given by user.
    /// </summary>
    public class LoopCommandChecker
    {
        /// <summary>
        /// Looks for starting and ending ending syntax fo Loop
        /// </summary>
        public void LoopValidity()
        {
            //do something
        }

        /// <summary>
        /// A method that loops through all the lines, calls loopvalidiy method
        /// passess appropriate integer indicating validity of the syntax
        /// </summary>
        /// <param name="lines">The code snippet provided by user</param>
        /// <returns>integer indicating validity of the syntax</returns>
        public int ReadAllLines(string lines)
        {
            int totalines;

            //perform a for loop that goes through
            //and passes it to loopvalidity
            return 0;

        }
    }
}

