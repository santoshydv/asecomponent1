﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PaintAppication;

namespace ShapeUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
        }
        [TestMethod]
        public void CheckTriangleShape()
        {
            var shapeFactoryCheck = new ShapeFactoryCheck();
            bool result = shapeFactoryCheck.isTriangle("triangle");

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void CheckShapeObject()
        {
            var factoryProducerCheck = new FactoryProducerCheck();

            bool result = factoryProducerCheck.isShape("shape");

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void CheckShapeObject()
        {
            var factoryProducerCheck = new FactoryProducerCheck();

            bool result = factoryProducerCheck.isShape("shape");

            Assert.AreEqual(true, result);
        }

        [TestMethod]
        public void CheckCircleShape()
        {
            var shapeFactoryCheck = new ShapeFactoryCheck();
            bool result = shapeFactoryCheck.isCircle("circle");

            Assert.AreEqual(true, result);
        }
    }
}
